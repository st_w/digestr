

md5iimpl <- function(data, len, raw) {
  if (is.raw(data)) {
    len <- length(data)
    data <- fastr.rawi.rawVToIntVBE(data)
  }

  stopifnot(is.integer(data))

  # define short names for functions
  OR <- fastr.rawi.rawOr
  AND <- fastr.rawi.rawAnd
  XOR <- fastr.rawi.rawXor
  NOT <- fastr.rawi.rawNot
  ADD <- fastr.rawi.rawAdd32LE
  ADD4 <- fastr.rawi.rawAdd32LE_4
  ROL <- fastr.rawi.rawRoL32LE
  asInt <- fastr.rawi.numToIntLE

  # length in bits
  b <- len * 8L

  # Step 1. Append Padding Bits
  data <- md5padi(data, len)

  # Step 2. Append Length
  data <- append(data, asInt(b %% 4294967296))
  data <- append(data, asInt(b %/% 4294967296))


  # Step 3. Initialize MD Buffer
  A <- asInt(0x67452301)
  B <- asInt(0xefcdab89)
  C <- asInt(0x98badcfe)
  D <- asInt(0x10325476)

  # Step 4. Process Message in 16-Word Blocks
  F <- function(x,y,z) OR(AND(x,y), AND(NOT(x), z))
  G <- function(x,y,z) OR(AND(x,z), AND(y, NOT(z)))
  H <- function(x,y,z) XOR(x, XOR(y, z))
  I <- function(x,y,z) XOR(y, OR(x, NOT(z)))

  T <- Map(function(x) asInt(trunc(4294967296 * abs(sin(x)))), 1:64)

  # Process each 16-word block
  for (i in 0L : (length(data)%/%16L-1L)) {

    # Copy block i into X
    X <- data[(1L+i*16L):(16L+i*16L)]

    # Save A as AA, B as BB, C as CC, and D as DD
    AA <- A
    BB <- B
    CC <- C
    DD <- D

    # 1. round
    S <- function(a,b,c,d, k,s,i) ADD(b, ROL(ADD4(a, F(b,c,d), X[[1+k]], T[[i]]), s))
    (A <- S(A,B,C,D,  0L,7L,1L))  ; (D <- S(D,A,B,C,  1L,12L,2L))  ; (C <- S(C,D,A,B,  2L,17L,3L))  ; (B <- S(B,C,D,A,  3L,22L,4L))
    (A <- S(A,B,C,D,  4L,7L,5L))  ; (D <- S(D,A,B,C,  5L,12L,6L))  ; (C <- S(C,D,A,B,  6L,17L,7L))  ; (B <- S(B,C,D,A,  7L,22L,8L))
    (A <- S(A,B,C,D,  8L,7L,9L))  ; (D <- S(D,A,B,C,  9L,12L,10L)) ; (C <- S(C,D,A,B, 10L,17L,11L)) ; (B <- S(B,C,D,A, 11L,22L,12L))
    (A <- S(A,B,C,D, 12L,7L,13L)) ; (D <- S(D,A,B,C, 13L,12L,14L)) ; (C <- S(C,D,A,B, 14L,17L,15L)) ; (B <- S(B,C,D,A, 15L,22L,16L))

    # 2. round
    S <- function(a,b,c,d, k,s,i) ADD(b, ROL(ADD4(a, G(b,c,d), X[[1+k]], T[[i]]), s))
    (A <- S(A,B,C,D,  1L,5L,17L)) ; (D <- S(D,A,B,C,  6L,9L,18L))  ; (C <- S(C,D,A,B, 11L,14L,19L)) ; (B <- S(B,C,D,A,  0L,20L,20L))
    (A <- S(A,B,C,D,  5L,5L,21L)) ; (D <- S(D,A,B,C, 10L,9L,22L))  ; (C <- S(C,D,A,B, 15L,14L,23L)) ; (B <- S(B,C,D,A,  4L,20L,24L))
    (A <- S(A,B,C,D,  9L,5L,25L)) ; (D <- S(D,A,B,C, 14L,9L,26L))  ; (C <- S(C,D,A,B,  3L,14L,27L)) ; (B <- S(B,C,D,A,  8L,20L,28L))
    (A <- S(A,B,C,D, 13L,5L,29L)) ; (D <- S(D,A,B,C,  2L,9L,30L))  ; (C <- S(C,D,A,B,  7L,14L,31L)) ; (B <- S(B,C,D,A, 12L,20L,32L))

    # 3. round
    S <- function(a,b,c,d, k,s,i) ADD(b, ROL(ADD4(a, H(b,c,d), X[[1+k]], T[[i]]), s))
    (A <- S(A,B,C,D,  5L,4L,33L)) ; (D <- S(D,A,B,C,  8L,11L,34L)) ; (C <- S(C,D,A,B, 11L,16L,35L)) ; (B <- S(B,C,D,A, 14L,23L,36L))
    (A <- S(A,B,C,D,  1L,4L,37L)) ; (D <- S(D,A,B,C,  4L,11L,38L)) ; (C <- S(C,D,A,B,  7L,16L,39L)) ; (B <- S(B,C,D,A, 10L,23L,40L))
    (A <- S(A,B,C,D, 13L,4L,41L)) ; (D <- S(D,A,B,C,  0L,11L,42L)) ; (C <- S(C,D,A,B,  3L,16L,43L)) ; (B <- S(B,C,D,A,  6L,23L,44L))
    (A <- S(A,B,C,D,  9L,4L,45L)) ; (D <- S(D,A,B,C, 12L,11L,46L)) ; (C <- S(C,D,A,B, 15L,16L,47L)) ; (B <- S(B,C,D,A,  2L,23L,48L))

    # 4. round
    S <- function(a,b,c,d, k,s,i) ADD(b, ROL(ADD4(a, I(b,c,d), X[[1+k]], T[[i]]), s))
    (A <- S(A,B,C,D,  0L,6L,49L)) ; (D <- S(D,A,B,C,  7L,10L,50L)) ; (C <- S(C,D,A,B, 14L,15L,51L)) ; (B <- S(B,C,D,A,  5L,21L,52L))
    (A <- S(A,B,C,D, 12L,6L,53L)) ; (D <- S(D,A,B,C,  3L,10L,54L)) ; (C <- S(C,D,A,B, 10L,15L,55L)) ; (B <- S(B,C,D,A,  1L,21L,56L))
    (A <- S(A,B,C,D,  8L,6L,57L)) ; (D <- S(D,A,B,C, 15L,10L,58L)) ; (C <- S(C,D,A,B,  6L,15L,59L)) ; (B <- S(B,C,D,A, 13L,21L,60L))
    (A <- S(A,B,C,D,  4L,6L,61L)) ; (D <- S(D,A,B,C, 11L,10L,62L)) ; (C <- S(C,D,A,B,  2L,15L,63L)) ; (B <- S(B,C,D,A,  9L,21L,64L))


    A <- ADD(AA, A)
    B <- ADD(BB, B)
    C <- ADD(CC, C)
    D <- ADD(DD, D)

  }

  # Step 5. Output
  if (raw)
    fastr.rawi.intVToRawVBE(c(A,B,C,D))
  else
    paste(format.hexmode(fastr.rawi.intVToRawVBE(c(A,B,C,D)), width = 2), collapse = '', sep = '')

}

md5padi <- function(o,l) {
  OR <- fastr.rawi.rawOr

  padSz <- 16L - (length(o)+2L) %% 16L
  r <- if (padSz == 16L) o else {
    pad <- integer(padSz)
    append(o, pad)
  }
  endIdx <- 1L + (l%%4L)
  endPos <- 1L + (l%/%4L)
  r[[endPos]] <- OR(r[[endPos]], switch(endIdx, 0x80L, 0x8000L, 0x800000L, as.integer(NA)))
  r
}

#testMD5 <- function(s = rndString(1234567)) digest(s, "md5", serialize = F)

